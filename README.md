# letItSnow

A nice little PySide2 (Qt) widget to bring the cold season's weather to your GUI

![letItSnow_demo01.gif](./letItSnow_demo01.gif)

## Installation

- Download [letItSnow.py](letItSnow.py)
- Add the path to its folder to your `PYTHONPATH`

## Usage

In your Python script `import letItSnow` and instantiate the Widget with the desired parent window/widget `letItSnow.LetItSnow(parent=self)`:

```python
import sys
from PySide2 import QtWidgets
import letItSnow


class Window(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.letItSnow = letItSnow.LetItSnow(parent=self)
        self.resize(500, 500)


def main():
    win = Window()
    win.exec_()


if __name__ == "__main__" and not QtWidgets.QApplication.instance():
    app = QtWidgets.QApplication()
    main()
    sys.exit(app.exec_())
else:
    main()
```


## Examples

Check out [example.py](./example.py) for a generic PySide example.

If You use [Nuke](https://www.foundry.com/products/nuke-family/nuke), check out the [example_nukeProperties.py](example_nukeProperties.py) to add some calming snowfall in the _Properties_ panel:

![letItSnow_demo-nuke-properties_v001.gif](./letItSnow_demo-nuke-properties_v001.gif)


## Caveats

You may notice some strange behavior when adding the `LetItSnow` Widget on top of a [QOpenGL](https://doc.qt.io/qtforpython-5/PySide2/QtOpenGL/QGLWidget.html?highlight=qglwidget) (In Nuke, that is for example the Node Graph or some special knob types such as LookupCurves). I have not yet found a fix for this. If you feel adventurous here are some places to start reading:

- About QOpenGLWidget limitations (transparency is one of them): https://doc.qt.io/qtforpython-5/PySide2/QtWidgets/QOpenGLWidget.html#limitations
- Bug report about using QOpenGLWidget as viewport for QGraphicsView: https://bugreports.qt.io/browse/QTBUG-53089
- Workaround for false drawing order: https://forum.qt.io/topic/98014/qgraphicsview-scene-paints-over-other-widgets/2
- About Z-depth of QGLWidgets https://stackoverflow.com/questions/8675065/qglwidget-for-graphics-and-qt-for-gui

## Support

If you found a bug or have a feature request, please use the issues page to submit it.
If you like this project and want to help me maintain it and others like it consider to:

<a href="https://www.buymeacoffee.com/fynn" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" ></a>

## License

[MIT License](./LICENSE)
