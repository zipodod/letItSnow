"""
file: example_nukeProperties.py
info: Example on how to add the widget to [Nuke's](https://www.foundry.com/products/nuke-family/nuke) Properties panel
      DISCLAIMER: Can't promise this is stable! It's definitely fun though.
      Just copy/paste this code into the Nuke Script Editor, run and enjoy the calming snowfall
"""
from PySide2 import QtWidgets
import letItSnow


def findWidget(name=None, strict=False, parentWidget=None):
    if not parentWidget:
        parentWidgets = QtWidgets.QApplication.instance().topLevelWidgets()
    else:
        parentWidgets = [parentWidget]
    for widget in parentWidgets:
        for child in widget.children():
            objName = child.objectName()
            if not strict:
                objName = objName.lower()
            if objName.find(name) != -1:
                yield child
            for _ in findWidget(name=name, strict=strict, parentWidget=child):
                yield _


def getPropertiesPanelWidget():
    for _ in findWidget(name="Properties.1", strict=True):
        return _


def getPropertiesPanelScrollArea():
    propPanelWidget = getPropertiesPanelWidget()
    if not propPanelWidget:
        print("Can't find properties panel widget")
        return None
    widget = propPanelWidget.findChild(QtWidgets.QScrollArea)
    return widget


def removeSnowFromPropertiesPanel(parentWidget=None):
    # type: (QtWidgets.QWidget) -> None
    for _ in parentWidget.children():
        if not isinstance(_, letItSnow.LetItSnow):
            continue
        print('Removing snow widget:', _)
        _.hide()
        _.deleteLater()
        _.setParent(None)
        break


def toggleSnowInPropertiesPanel(state=None):
    # NOTE: Need to save propertiesPanelWidget her (calling function) so it doesn't get garbage-collected \
    #       before removeSnowFromPropertiesPanel can do its thing. This is only a problem in pre-Nuke13
    propertiesPanelWidget = getPropertiesPanelWidget()
    parentWidget = getPropertiesPanelScrollArea()
    if not parentWidget:
        print("Can't find properties panel scroll area")
        return
    # Delete previously added snowy widget if one exists
    # NOTE: This won't work if you reloaded the letItSnow module after adding the LetItSnow widget
    snowWidget = parentWidget.findChild(letItSnow.LetItSnow)
    if snowWidget:
        print("Removing snow")
        removeSnowFromPropertiesPanel(parentWidget=parentWidget)
        return
    if state is False:
        return
    print("Adding snow")
    parentWidget.snowy = letItSnow.LetItSnow(parent=parentWidget)
    parentWidget.snowy.show()


if __name__ == "__main__":
    toggleSnowInPropertiesPanel()
